﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Model
{
    internal class productModel
    {
        public int Id;
        public string Name;
        public int Price;
        public productModel(int id, string name, int price)
        {
            this.Id = id;
           this.Name = name;
           this.Price = price;
        }
    }
}
