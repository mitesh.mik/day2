﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registerfunctionality.Model
{
    internal class Register
    {
        public string username;
        public string password;
        public string email;

        public Register(string name, string pass, string email)
        {
            this.username=name;
            this.email = email;
            this.password = pass;
        }
    }
}
